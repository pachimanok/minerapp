// Add Firebase products that you want to use
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js');

// Firebase SDK
firebase.initializeApp({
    apiKey: "AIzaSyA9qf9K0v4RDjHwRTReOp1l-YCzOr2XuH4",
    authDomain: "minerappnotifications.firebaseapp.com",
    projectId: "minerappnotifications",
    storageBucket: "minerappnotifications.appspot.com",
    messagingSenderId: "25912625827",
    appId: "1:25912625827:web:00b89b7f955e690d408d16",
});

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log("Message has received : ", payload);
    const title = "First, solve the problem.";
    const options = {
        body: "Push notificaiton!",
        icon: "/icon.png",
    };
    return self.registration.showNotification(
        title,
        options,
    );
});

