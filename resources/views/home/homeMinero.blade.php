@include('layouts.header')
<body>
    @include('layouts.barralateral')
    @include('layouts.user')

    <div class="container-fluid" style="margin-top: 8rem !important;">
        <div class="header bg-white pb-6">
            <div class="header-body">
                <div class="row mt-5 ml-4 mr-4 text-center"
                    style="height: 5rem; border-radius: 10px; background: #e2f4f5;">
                    <div class="col-sm-4 pl-3 pr-0 mt-3" style="max-width: 33%;">
                        <div class="card text-center" style="background: #e2f4f5; border-style: revert;">
                            <h6>Puntos Miner</h6>
                            <h5 class="pt-0"> {{ $mineros->pts }}</h5>
                        </div>
                    </div>
                    <div class="col-sm-4 pl-1 pr-3 mt-3" style="max-width: 34%;">
                        <a href="/billetera">
                            <div class="card text-center"
                                style="height: 3rem; background: #e2f4f5; border-style: revert;">
                                <h6>Total Recaudado:</h6>
                                <h5 class="pt-0"> ${{ $cob }} </h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 pl-1 pr-3 mt-3" style="max-width: 33%;">
                        <a href="#">
                            <div class="card text-center"
                                style="height: 3rem; background: #e2f4f5; border-style: revert;">
                                <h6>Total Retirado:</h6>
                                <h5 class="pt-0"> ${{ $des }} </h5>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="d-flex mt-3">
                    <div class="col-sm-6">
                        <h2 style="color:black;">Minas</h2>
                    </div>
                    <div class="col-sm-6 text-right  pt-1">
                        <a href="" class="mt-3">
                            <h6 style="
                            font-size: initial;
                        " class="align-middle"></h6>
                        </a>
                    </div>
                </div>
                <div class="row row-cols-5  ml-2 mr-2">
                    @foreach ($minas as $mina)
                        <div class="col p-1">
                            <div class="card text-white text-center bg-success" style="
                                width: 66px;
                                height: 66px;
                                border-radius: 10px;
                                border: none;
                                margin: 0px 3px;">
                                <img src="{{ asset('img/miner-blanco-29.svg') }}" alt="">
                            </div>
                            <p class="text-success" style="font-size: x-small;
                            font-weight: 800;
                            text-align: center;
                            "> {{ $mina->titulo }}</p>
                        </div>
                    @endforeach
                    <div class="col p-1">
                        <a href="/mina/create" class="card text-suceess text-center" style="
                        width: 66px;
                        height: 66px;
                        border-radius: 10px;
                        border-style: dashed;
                        border-color: slategrey !important;
                        border-width: 2px;
                        margin: 0px 3px;
                        padding-top: 25%;">
                            <i class="fas fa-plus" style="font-size: xx-large;
                        color: slategrey; margin-right: 0;"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="d-flex mt-3">
                <div class="col-sm-6">
                    <h2 style="color:black;">Mi Proyecto</h2>
                </div>
                <div class="col-sm-6 text-right  pt-1">
                    {{-- 
                    <a href="" class="mt-3">
                        <a data-bs-toggle="offcanvas" 
                        data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom" style="
                        font-size: initial;
                        " class="align-middle">Ver desafíos</a>
                    </a> --}}
                </div>
            </div>
            <div class="row">
                <div class="card  ml-3 mr-3" data-bs-toggle="offcanvas" 
                data-bs-target="#offcanvasBottom" aria-controls="offcanvasBottom" style="height: 6rem; border-radius:10px;width: fit-content; border:none;box-shadow: 1px 8px 19px -7px rgba(0,0,0,0.37);
                    -webkit-box-shadow: 1px 8px 19px -7px rgba(0,0,0,0.37);
                    -moz-box-shadow: 1px 8px 19px -7px rgba(0,0,0,0.37);">
                    <div class="d-flex mt-3 mb-3">
                        <div class="col text-center pt-4">
                            <i class="fas fa-trophy" style="font-size: large; color: #1f538a;"></i>
                        </div>
                        <div class="col-10">
                            <p>¡Ganá Puntos! Para avanzar como Minero, tendrás desafios disponibles cada semana.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex mt-3">
                <div class="col-sm-8">
                    <h2 style="color:black;">Descubrí empresas</h2>
                </div>
                <div class="col-sm-4 text-right  pt-1" style="width: 35%;">
                    <a href="/minar" class="mt-3">
                        <h6 style="
                        font-size: initial;
                    " class="align-middle">Ver todas</h6>
                    </a>
                </div>
            </div>
            <div class="row row-cols-3">
                @foreach ($alianza as $alianza)
                    <div class="col-sm-4">
                        <a href="alianza/{{ $alianza->id }}">
                            <div class="card" style="border:none;">
                                <img src="{{ asset('/img/avatar/' . $alianza->avatar) }}" style="object-fit: cover;      width: 6rem;
                                height: 6rem;" alt="" class="img-fluid">
                                <h5 style="padding:0; margin-top:5px;">{{ $alianza->nombre_fantasia }}</h5>
                                <h6>{{ $alianza->rubro }}</h6>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="d-flex mt-3">
                <div class="col-sm-8">
                    <h2 style="color:black;">Conocé más</h2>
                </div>
                <div class="col-sm-4 text-right  pt-1" style="width: 35%;">
                    <a href="/minar" class="mt-3">
                        <h6 style="
                        font-size: initial;
                    " class="align-middle"></h6>
                    </a>
                </div>
            </div>
            <div class="dropdown-divider"></div>
            <div class="accordion" id="accordionExample">
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        <i class="far fa-question-circle"></i>¿Cómo funciona MinerApp?
                    </button>
                  </h2>
                  <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        Es simple: debés realizar los Desafíos semanales de contenidos para sumar puntos y tus compras (o de tus familiares y cercanos) en las tiendas adheridas para ganar dinero, cargando una imagen de los tickets. Cada vez que acumules 30 puntos o más, podrás realizar un retiro de dinero a tu cuenta Banco del Sol. 
                        Los locales adheridos son de productos de consumo habitual. La idea es que invites a tus familiares a comprar en dónde les indiques para poder recibir un monto por cada compra o venta referida. Buscamos extendernos a más rubros en el futuro, ¡estáte atento!                        
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <i class="far fa-question-circle"></i>¿Cómo gestionar y utilizar minas?
                    </button>
                  </h2>
                  <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        Desde la sección Inicio, podés cargar las minas que creas necesarias. Estas “Minas” son el lugar físico, son las casas en las que algún integrante compra productos de consumo habitual a través tuyo. Puede ser tu propia casa, o de otros familiares y amigos. 
                        El Título que debes ingresar es únicamente para que puedas reconocer de forma rápida a qué casa hace referencia. Al Minar una compra, te pedirá que selecciones la Mina utilizada y, además, que aclares los datos del responsable de la compra. 
                    </div>
                  </div>
                </div>
              </div>
        </div>

        {{-- Notificaciones Push --}}

        {{-- <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                        <button onclick="initNotification()"
                            class="btn btn-danger btn-flat">Generate Device Token
                        </button>
                    <div class="card mt-3">
                        <div class="card-body">
                            @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                            @endif
                            <form action="/send-notification" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control" >
                                </div>
        
                                <div class="form-group">
                                    <label>Body</label>
                                    <textarea name="body" class="form-control" ></textarea>
                                </div>
                                
                                <div class="form-group">
                                  <button type="submit" class="btn btn-dark btn-block">Send</button>
                                </div>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form action="/save-device-token" method="POST" name="tokenform">
            @csrf
            <input type="hidden" name="tokenin" id="tokenin">
        </form> --}}
        
        <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
        <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase.js"></script>
        <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js"></script>
        <script>
            var firebaseConfig = {
                apiKey: "AIzaSyA9qf9K0v4RDjHwRTReOp1l-YCzOr2XuH4",
                authDomain: "minerappnotifications.firebaseapp.com",
                projectId: "minerappnotifications",
                storageBucket: "minerappnotifications.appspot.com",
                messagingSenderId: "25912625827",
                appId: "1:25912625827:web:00b89b7f955e690d408d16",
            };
        
            firebase.initializeApp(firebaseConfig);
            const messaging = firebase.messaging();
        
            /* function initNotification() {
                messaging
                    .requestPermission().then(function () {
                        return messaging.getToken()
                    }).then(function (token) {
                        guardarToken(token);
                    }).catch(function (error) {
                        console.log(error);
                    });
            } */
        
            
            let enableForegroundNotification=true;
            messaging.onMessage(function(payload){
                console.log("mensaje recibido");
                if(enableForegroundNotification){
                    const {title, ...options}=JSON.parse(payload.data.notification);
                    navigator.serviceWorker.getRegistrations().then( registration =>{
                        registration[0].showNotification(title, options);
                    });
                }
            });
        
            messaging.onMessage(function (payload) {
            const title = payload.notification.title;
            const options = {
                body: payload.notification.body,
                icon: payload.notification.icon,
            };
            new Notification(title, options);
            });
        
            /* function guardarToken(token){
                    document.getElementById("tokenin").value = token;
                    document.tokenform.submit();
                } */
        </script>








        @include('layouts.navegacion')
