<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nuevo Usuario en MinerApp</title>
</head>
<body>
    <h3>Se ha registrado un nuevo usuario en MinerApp</h3>
    <ul>
        <li>Usuario: {{ $msg['name']}}</li>
        <li>E-mail: {{ $msg['email']}}</li>
    </ul>

    Saludos, 
    Equipo de MinerApp
</body>
</html>