<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Confirmacion de minado de {{ $msg['minero'] }}</title>
</head>
<body>
    <h1>¡Gracias por tu ayuda! Desde MinerApp, queremos confirmar la compra realizada a través de 
        {{ $msg['minero'] }}. Esta compra tuvo una devolución de ${{ $msg['comision'] }} para {{ $msg['minero'] }} 
        y es de gran ayuda para continuar su aprendizaje y armado de su Proyecto de Vida.</h1>
    <br>
    <p>Gracias de prueba</p>
</body>
</html>