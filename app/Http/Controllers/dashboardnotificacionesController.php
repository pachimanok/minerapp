<?php

namespace App\Http\Controllers;
use App\Models\notification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;


class dashboardnotificacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = Auth::user();
        $notificaciones['notificaciones'] = notification::All();
        return view('dashboard.notificaciones', $notificaciones)->with('user', $usuario);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usuario = Auth::user();
        $usuarios = DB::table('users')->get();
        return view('formularios.createdashboardNotificaciones')->with('usuarios', $usuarios)->with('user', $usuario);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destinatario = $request['destinatario'];
        
        if($destinatario == '0'){

            $usuarios = DB::table('users')->get();

            foreach($usuarios as $usuario){

                if($usuario->role == "minero"){

                    $imagen = $request['imagen'];
                    if($imagen != null){
                        $extencion = $imagen->getClientOriginalExtension();
                        $name = $imagen->getClientOriginalName();
                        $imagen = Image::make($imagen);
                        $imagen->resize(300, 300);
                        $imagen->encode($extencion);
                        $path = public_path('img/notificaciones/' . $name);
                        $imagen->save($path);
                        $imagen = $name;
                    }
                    $notificacion = new notification();
                    $notificacion->destinatario = $usuario->name;
                    $notificacion->titulo = $request['titulo'];
                    $notificacion->descripcion = $request['descripcion'];
                    $notificacion->link = $request['link'];
                    $notificacion->tipo = $request['tipo'];
                    $notificacion->imagen = $imagen;
                    $notificacion->save();

                    /* Notificaciones push enviar notificacion */

                    $deviceToken = DB::table('users')->where('name', '=', $destinatario)->get('device_token')->pluck('device_token')->all();
                    
                    $url = 'https://fcm.googleapis.com/fcm/send';
                    $DeviceToekn = User::whereNotNull('device_token')->pluck('device_token')->all();
                    
                    $FcmKey = 'AAAABgiDSqM:APA91bFqnU3dftaOd9puP7kmUNvo2k9y3KUFXcBIpTN1n26GUh5oPMLQz07viCxEynp6NbCr3khZBDvBPHxiDL06xKK9IMVWumg0hixNcDYuSUKIBUd338d8PMeQi43eDc8ok5pU3rmp';
            
                    $data = [
                        "registration_ids" => $deviceToken,
                        "notification" => [
                            "title" => $request['titulo'],
                            "body" => $request['descripcion'],
                            "link" => 'https://minerapp.com.ar/', 
                            "icon" => 'http://rail.ar/android-icon-512x512.png', 
                            "badge" => 'http://rail.ar/android-icon-512x512.png', 
                        ]
                    ];

                    $RESPONSE = json_encode($data);
                
                    $headers = [
                        'Authorization:key=' . $FcmKey,
                        'Content-Type: application/json',
                    ];
                
                    // CURL
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $RESPONSE);

                    $output = curl_exec($ch);
                    if ($output === FALSE) {
                        die('Curl error: ' . curl_error($ch));
                    }        
                    curl_close($ch);
                    
                    //dd($output); 
                }
            }
        } else {
                    $imagen = $request['imagen'];
                    if($imagen != null){
                        $extencion = $imagen->getClientOriginalExtension();
                        $name = $imagen->getClientOriginalName();
                        $imagen = Image::make($imagen);
                        $imagen->resize(300, 300);
                        $imagen->encode($extencion);
                        $path = public_path('img/notificaciones/' . $name);
                        $imagen->save($path);
                        $imagen = $name;
                    }
                    $notificacion = new notification();
                    $notificacion->destinatario = $request['destinatario'];
                    $notificacion->titulo = $request['titulo'];
                    $notificacion->descripcion = $request['descripcion'];
                    $notificacion->link = $request['link'];
                    $notificacion->tipo = $request['tipo'];
                    $notificacion->imagen = $imagen;
                    $notificacion->save();

                    /* Notificaciones push enviar notificacion */

                    $deviceToken = DB::table('users')->where('name', '=', $destinatario)->get('device_token')->pluck('device_token')->all();
                    
                    $url = 'https://fcm.googleapis.com/fcm/send';
                    $DeviceToekn = User::whereNotNull('device_token')->pluck('device_token')->all();
                    
                    $FcmKey = 'AAAABgiDSqM:APA91bFqnU3dftaOd9puP7kmUNvo2k9y3KUFXcBIpTN1n26GUh5oPMLQz07viCxEynp6NbCr3khZBDvBPHxiDL06xKK9IMVWumg0hixNcDYuSUKIBUd338d8PMeQi43eDc8ok5pU3rmp';
            
                    $data = [
                        "registration_ids" => $deviceToken,
                        "notification" => [
                            "title" => $request['titulo'],
                            "body" => $request['descripcion'],
                            "link" => 'https://minerapp.com.ar/', 
                            "icon" => '/assets/img/android-icon-512x512.png',  
                        ]
                    ];

                    $RESPONSE = json_encode($data);
                
                    $headers = [
                        'Authorization:key=' . $FcmKey,
                        'Content-Type: application/json',
                    ];
                
                    // CURL
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $RESPONSE);

                    $output = curl_exec($ch);
                    if ($output === FALSE) {
                        die('Curl error: ' . curl_error($ch));
                    }        
                    curl_close($ch);
                    
                    //dd($output); 
            
        }
        $usuario = Auth::user();

        $listaNotificaciones = DB::table('notifications')->get();

        return view('dashboard.notificaciones')->with('notificaciones', $listaNotificaciones)->with('user', $usuario);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $usuario = Auth::user();
        $notificacion = notification::find($id);       
        return view('formularios.verdashboardNotificacion')->with('notificacion', $notificacion)->with('user', $usuario);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notificacion = notification::find($id);
        $notificacion->estado = $request['estado'];
        $notificacion->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notificacion = notification::find($id);
        $notificacion->delete();
        return redirect('/dashboardnotificaciones');
    }
}
