<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class NotiPushController extends Controller
{
    public function saveDeviceToken(Request $request)
    {
        $usuario = Auth::user();
        $usuario->name;
        $usuario->device_token = $request->tokenin;
        $usuario->save();
        //return response()->json(['Token stored.']);
        return redirect('/perfil');
    }

    public function sendNotification(Request $request)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $DeviceToekn = User::whereNotNull('device_token')->pluck('device_token')->all();
          
        $FcmKey = 'AAAABgiDSqM:APA91bFqnU3dftaOd9puP7kmUNvo2k9y3KUFXcBIpTN1n26GUh5oPMLQz07viCxEynp6NbCr3khZBDvBPHxiDL06xKK9IMVWumg0hixNcDYuSUKIBUd338d8PMeQi43eDc8ok5pU3rmp';
  
        $data = [
            "registration_ids" => $DeviceToekn,
            "notification" => [
                "title" => $request->title,
                "body" => $request->body,
                "link" => 'https://minerapp.com.ar/', 
                "icon" => '/assets/img/android-icon-512x512.png',
            ]
        ];

        $RESPONSE = json_encode($data);
    
        $headers = [
            'Authorization:key=' . $FcmKey,
            'Content-Type: application/json',
        ];
    
        // CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $RESPONSE);

        $output = curl_exec($ch);
        if ($output === FALSE) {
            die('Curl error: ' . curl_error($ch));
        }        
        curl_close($ch);     
        
        //dd($output); 
    }
}
